/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:44:16 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/24 14:50:05 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int		ft_pow(int val, int pow)
{
	int		res;

	res = 1;
	while (pow > 0)
	{
		if (pow % 2 == 1)
			res *= val;
		val *= val;
		pow /= 2;
	}
	return (res);
}

static int		ft_nbrchk_fd(int n, int fd)
{
	if (n == 0)
	{
		ft_putchar_fd('0', fd);
		return (0);
	}
	if (n == -2147483648)
	{
		ft_putstr_fd("-2147483648", fd);
		return (0);
	}
	return (1);
}

void			ft_putnbr_fd(int n, int fd)
{
	int		tmp;
	int		i;

	if (!ft_nbrchk_fd(n, fd))
		return ;
	if (n < 0)
	{
		ft_putchar_fd('-', fd);
		n *= -1;
	}
	tmp = n;
	i = 0;
	while (tmp)
	{
		tmp /= 10;
		i++;
	}
	while (i)
	{
		ft_putchar_fd('0' + (char)(n / ft_pow(10, i - 1)), fd);
		n %= ft_pow(10, i - 1);
		i--;
	}
}
