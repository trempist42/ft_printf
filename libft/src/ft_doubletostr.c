/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_doubletostr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 14:08:33 by rafalmer          #+#    #+#             */
/*   Updated: 2019/02/21 19:42:54 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <libftprintf.h>
#include <stdio.h>
#include <math.h>

typedef union	u_double
{
	double		dbl;
	char		data[sizeof(double)];
}				t_double;

static void		dump_double(t_double d)
{
	int			exp;
	int			sign;
	double		res;
	long long	mant;

	sign = (d.data[7] & 0x80) >> 7;
	exp = ((d.data[7] & 0x7F) << 4) | ((d.data[6] & 0xF0) >> 4);
	mant = ((((d.data[6] & 0x0F) << 8) | (d.data[5] & 0xFF)) << 8) | (d.data[4] & 0xFF);
	mant = (mant << 32) | ((((((d.data[3] & 0xFF) << 8) | (d.data[2] & 0xFF)) << 8) | (d.data[1] & 0xFF)) << 8) | (d.data[0] & 0xFF);
	res = pow(-1, sign) * (1 + (mant / pow(2, 52))) * pow(2, exp - 1023);
	printf("%f", res);
}

char			*ft_doubletostr(double n)
{
	t_double	nbr;
	nbr.dbl = n;
	dump_double(nbr);
	return (NULL);
}
