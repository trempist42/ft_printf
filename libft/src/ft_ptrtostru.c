/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ptrtostru.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <rafalmer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/20 17:59:05 by rafalmer          #+#    #+#             */
/*   Updated: 2019/01/22 19:28:20 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_ptrtostru(unsigned long long int n)
{
	char	*res;
	int		i;

	i = 2;
	if (!(res = (char *)malloc(sizeof(char) * 16)))
		return (NULL);
	n <<= 16;
	*(res + 15) = '\0';
	*(res) = '0';
	*(res + 1) = 'x';
	while (i < 14)
	{
		*(res + i) = (n >> 60) < 10 ? (n >> 60) + '0' : (n >> 60) - 10 + 'A';
		n <<= 4;
		i++;
	}
	return (res);
}
