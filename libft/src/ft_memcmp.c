/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 14:41:20 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/24 14:49:29 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t			i;
	unsigned char	c1;
	unsigned char	c2;
	char			*p1;
	char			*p2;

	i = 0;
	p1 = (char *)s1;
	p2 = (char *)s2;
	while (i < n)
	{
		c1 = (unsigned char)p1[i];
		c2 = (unsigned char)p2[i];
		if (c1 != c2)
			return (c1 - c2);
		i++;
	}
	return (0);
}
