/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 15:30:04 by rafalmer          #+#    #+#             */
/*   Updated: 2019/01/30 16:48:03 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libftprintf.h>
#include <stdio.h>

static char	*ft_chartostr(const char s1)
{
	char	*res;
	char	*tmp;

	if (!(res = (char *)malloc(sizeof(char) * 2)))
		return (NULL);
	tmp = res;
	*tmp++ = s1;
	*tmp = '\0';
	return (res);
}

static int	ft_handler(char *str, va_list *ap, t_list **tmplst)
{
	if (*str == 's')
	{
		if (!((*tmplst)->content = ft_strdup(va_arg(*ap, char *))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'd' || *str == 'i')
	{
		if (!((*tmplst)->content = ft_itoa(va_arg(*ap, int))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'p')
	{
		if (!((*tmplst)->content = ft_ptrtostrl(va_arg(*ap, unsigned long long int))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'P')
	{
		if (!((*tmplst)->content = ft_ptrtostru(va_arg(*ap, unsigned long long int))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'x')
	{
		if (!((*tmplst)->content = ft_dectohexl(va_arg(*ap, int))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'X')
	{
		if (!((*tmplst)->content = ft_dectohexu(va_arg(*ap, int))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'o')
	{
		if (!((*tmplst)->content = ft_dectooct(va_arg(*ap, int))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'u')
	{
		if (!((*tmplst)->content = ft_ltoa(va_arg(*ap, unsigned int))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'c')
	{
		if (!((*tmplst)->content = ft_chartostr(va_arg(*ap, int))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == '%')
	{
		if (!((*tmplst)->content = ft_chartostr('%')))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	else if (*str == 'f')
	{
		if (!((*tmplst)->content = ft_doubletostr(va_arg(*ap, double))))
			return (0);
		if (!((*tmplst)->next = ft_lstnew(NULL, 0)))
			return (0);
		(*tmplst) = (*tmplst)->next;
	}
	return (1);
}

int		ft_printf(const char *format, ...)
{
	va_list	ap;
	char	*str;
	char	*tmp;
	char	*frm;
	t_list	*lst;
	t_list	*tmplst;
	t_list	*tmplst1;

	frm = ft_strdup(format);
	if (!(lst = ft_lstnew(NULL, 0)))
		return (-1);
	tmplst = lst;
	tmp = frm;
	va_start(ap, format);
	while (*tmp)
	{
		if ((str = ft_strchr(tmp, '%')))
		{
			*str = '\0';
			if (!(tmplst->content = ft_strdup(tmp)))
				return (-1);
			if (!(tmplst->next = ft_lstnew(NULL, 0)))
				return (-1);
			tmplst = tmplst->next;
			if (!(ft_handler(str + 1, &ap, &tmplst)))
				return (-1);
			tmp = str + 2;
		}
		else
		{
			if (!(tmplst->content = ft_strdup(tmp)))
				return (-1);
			tmplst->next = ft_lstnew(NULL, 0);
			break ;
		}
	}
	va_end(ap);
	tmplst = lst;
	while (tmplst)
	{
		ft_putstr(tmplst->content);
		tmplst = tmplst->next;
	}
	tmplst = lst;
	while (tmplst)
	{
		free(tmplst->content);
		tmplst1 = tmplst->next;
		free(tmplst);
		tmplst = tmplst1;
	}
	free(frm);
	return (0);
}
