/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 15:29:02 by rafalmer          #+#    #+#             */
/*   Updated: 2019/01/30 19:07:00 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <libftprintf.h>
#include <float.h>

int		main(void)
{
	char	*str;
	double	n;

	n = DBL_MAX;
	str = malloc(1);
	ft_printf("Hello, %f\n", n);
	printf("Hello, %f\n", n);
	free(str);
	return (0);
}
